extern crate tokio_service;
extern crate futures;

#[macro_use]
extern crate hyper;
extern crate tokio_hyper as http;

extern crate rustc_serialize;

use tokio_service::Service;
use futures::{Async, Future, finished, BoxFuture};
use std::thread;
use std::time::Duration;
use rustc_serialize::json;
use hyper::RequestUri;

#[derive(RustcEncodable, RustcDecodable)]
struct Named {
	name: String,
	id: u64,
	data: Data
}

#[derive(RustcEncodable, RustcDecodable)]
struct Data {
	data: std::vec::Vec<String>,
}

impl Named {
	fn new(name:String, id: u64, strings: std::vec::Vec<String>) -> Self {
		Named{name: name, id: id, data: Data { data: strings }}
	}
}

#[derive(Clone)]
struct MyService;

impl Service for MyService {
	type Request = http::Message<http::Request>;
	type Response = http::Message<http::Response>;
	type Error = http::Error;
	type Future = BoxFuture<Self::Response, http::Error>;

	fn call(&self, req: Self::Request) -> Self::Future {
			let uri = req.head().uri();
			
			match *uri {
				RequestUri::AbsolutePath{ref path, ref query} => {
					match path.to_owned().as_ref() {
						"/" => println!("Index requested!"),
						"/favicon.ico" => (), // ignore favicon
						_ => { 
							println!(
								"{:?} Requested path: {:?} query: {:?}",
								req.head().method(),
								path,
								query
							);
						}
					}
				},
				_ => {}
			}
			
			let named = Named::new("Daniel".to_string(), 42, vec!["one".to_string()]);
			let json = json::encode(&named).unwrap();

			// Create the HTTP response with the body
			let resp = http::Message::new(http::Response::ok())
					.with_body(json.into_bytes());

			// Return the response as an immediate future
			finished(resp).boxed()
	}
}

pub fn main() {
	http::Server::new()
			.bind("0.0.0.0:8080".parse().unwrap())
			.serve(|| MyService)
			.unwrap();

	thread::sleep(Duration::from_secs(1_000_000));
}
